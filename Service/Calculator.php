<?php


namespace Service;

use Api\NbrbApi;
use Items\Currency;

/**
 * Class Calculator
 * @package Service
 */
class Calculator
{
    /**
     * @var float
     */
    private $fromRate;

    /**
     * @var float
     */
    private $toRate;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @var NbrbApi
     */
    private $nbrbApi;

    /**
     * Calculator constructor.
     * @param $from
     * @param $to
     */
    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
        $this->nbrbApi = new NbrbApi();
    }

    /**
     * @return float
     */
    public function getResult()
    {
        $this->requestOfficialRate();

        return $this->toRate / $this->fromRate;
    }

    /**
     * @param $abbreviation
     * @return mixed
     */
    private function getRate($abbreviation)
    {
        $currencies = $this->nbrbApi->getCurrenciesTodays();
        $currency = array_filter($currencies, function(Currency $currency) use ($abbreviation) {
            return $currency->abbreviation === $abbreviation;
        });
        $currency = array_pop($currency);

        return $currency->officialRate;
    }

    private function requestOfficialRate()
    {
        $this->fromRate = $this->from == 'BYN' ? 1 : $this->getRate($this->from);
        $this->toRate = $this->getRate($this->to);
    }
}