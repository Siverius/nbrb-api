<?php


namespace Mapper;

use Items\Currency;

/**
 * Class CurrencyMapper
 * @package Mapper
 */
class CurrencyMapper
{
    /**
     * @param array $data
     * @return Currency
     */
    public static function map($data)
    {
        $currency = new Currency();
        $currency->id = $data['Cur_ID'];
        $currency->date = $data['Date'];
        $currency->abbreviation = $data['Cur_Abbreviation'];
        $currency->scale = $data['Cur_Scale'];
        $currency->name = $data['Cur_Name'];
        $currency->officialRate = $data['Cur_OfficialRate'];

        return $currency;
    }
}
