<?php

namespace Type;

use \Exception;

/**
 * Class RequestCurrency
 * @package Type
 */
class RequestCurrency
{
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $to;

    /**
     * RequestCurrency constructor.
     * @param string $from
     * @param string $to
     */
    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getFrom()
    {
        $array = explode('=', $this->from);

        if($array[0] !== '--from') {
            throw new \Exception('Please, use flag "--from" to insert currency abbreviation');
        }

        return $array[1];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getTo()
    {
        $array = explode('=', $this->to);

        if($array[0] !== '--to') {
            throw new \Exception('Please, use flag "--to" to insert currency abbreviation');
        }

        return $array[1];
    }
}