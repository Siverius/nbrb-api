<?php

namespace Transport;

class Curl
{
    public function get($url)
    {
        $response = $this->send($url);

        return json_decode($response, true);
    }

    public function send($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }
}
