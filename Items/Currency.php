<?php


namespace Items;


class Currency
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $date;

    /**
     * @var string
     */
    public $abbreviation;

    /**
     * @var int
     */
    public $scale;

    /**
     * @var string
     */
    public $name;

    /**
     * @var float
     */
    public $officialRate;
}
