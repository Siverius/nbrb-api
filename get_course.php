#!/usr/bin/php
<?php

spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

$from = $argc === 2 ? "--from=BYN" : $argv[1];

$to = array_pop($argv);

$requestCurrency = new Type\RequestCurrency($from, $to);

try {
    $calculator = new Service\Calculator($requestCurrency->getFrom(), $requestCurrency->getTo());
} catch (Exception $e) {
    exit($e->message());
}

print_r($calculator->getResult());
