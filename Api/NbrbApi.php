<?php


namespace Api;

use Transport\Curl;
use Mapper\CurrencyMapper;

class NbrbApi
{
    private $transport;

    public function __construct()
    {
        $this->transport = new Curl();
    }

    public function getCurrenciesTodays()
    {
        $response = $this->transport->get('http://www.nbrb.by/api/exrates/rates?periodicity=0');

        return array_map(
            function ($data) {
                return CurrencyMapper::map($data);
            }, $response);
    }
}
